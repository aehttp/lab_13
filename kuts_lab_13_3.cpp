/* file name: kuts_lab_13_3 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 05.02.2022
*дата останньої зміни 05.02.2022
*Лабораторна №13
*завдання : Усі елементи кожної діагоналі, що
паралельні головній діагоналі квадратної
матриці та розташовані під головною
діагоналлю зростанням гномів.
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
using namespace std;
void input(int *a,int n){
	int i,j;
	printf("Сформований масив :\n");
	for(i=0 ; i<n ; i++){
		for(j=0; j<n ; j++){
			*(a+ i*n + j)=rand()%100-50;
			printf("%4d ",*(a+ i*n + j));
		}
		cout<<endl;
	}
}
void sort(int a[],int n){
	int i,j, k;
	int tmp;
	for (k = n / 2; k > 0; k /= 2){

		for (i = k; i < n; i++){
			tmp = a[i];
			for (j = i; j >= k; j -= k){
				if (tmp > a[j - k])
					a[j] = a[j - k];
				else
					break;
			}
			a[j] = tmp;
			}
	}

}
void diag(int *a,int n){
	int size=0,k=0,i,j;
	for (i=n-1 ; i>0; i--){
		size+=i;
	}
	int b[size];
	printf("\n mass : ");
	for(i=0; i<n; i++){
		for(j=0;j<(i);j++){
			b[k]=*(a+ i*n + j);
			k++;
		}
	}
	sort(b,size);
	int r,s=-1,pi,pj;
  	for (r = 0; r < (2*n-1); r++){
      	if (r<n){
	        pi=0;
	        pj=n-r-1;
	    }
	      else{
	        pi=r-(n-1);
	        pj=0;
	    }
		for (; (pi<n)&(pj<n);pi++,pj++) {
		    	if(pj<pi){
		    		s++;
		      		*(a+ pi*n + pj)=b[s];
		    	}
	    }
    }
	printf("Сортований масив :\n");
	for(i=0 ; i<n ; i++){
		for(j=0; j<n ; j++){
			printf("%4d ",*(a+ i*n + j));
		}
		cout<<endl;
	}
}
int main(){
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i,j,k,n;
	printf("введіть розмір масиву : ");
	cin>>n;
	int a[n][n];
	input(a[0],n);
	diag(a[0],n);
	system("pause");
	return 0;
}

