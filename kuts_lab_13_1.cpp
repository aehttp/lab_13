/* file name: kuts_lab_13_1 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 05.02.2022
*дата останньої зміни 05.02.2022
*Лабораторна №13
*завдання :  Обчислити та вивести середнє арифметичне непарних елементів масиву c6(n).
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
using namespace std;
void random(int m[], int n){
	int i;
	printf("Сформований масив :\n");
	for(i=0 ; i<n ; i++){
		m[i]=rand()%100-50;
		printf("%4d",m[i]);
	}
}
void ser(int m[], int n){
	int i,k=0,s=0;
	double sr;
	for(i=0;i<n;i++){
		if(m[i]%2 != 0){
			k++;
			s+=m[i];
		}
	}
	sr= (double)s/(double)k;
	printf("\nСереднє ариф. непарних елементів масиву : %0.1f \n",sr);
}
int main(){
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int n;
	printf("Введіть розмір массиву c6 : ");
	cin>>n;
	int c6[n];
	random(c6,n);
	ser(c6,n);
	system("pause");
	return 0;
}

