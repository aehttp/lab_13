/* file name: kuts_lab_13_4 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 05.02.2022
*дата останньої зміни 05.02.2022
*Лабораторна №13
*завдання : Створити та вивести одномірний масив з сум додатних елементів кожної
паралельної діагоналі, які вище головної діагоналі матриці v35(n,m).
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
using namespace std;
void input(int *a,int n){
	int i,j;
	printf(" сформований масив :\n");
	for(i=0 ; i<n ; i++){
		for(j=0; j<n ; j++){
			*(a+ i*n + j)=rand()%100-50;
			printf("%4d ",*(a+ i*n + j));
		}
		cout<<endl;
	}
}
void diag(int *a,int n){
	int k=0,i,j;
	int b[n-1];
	for(i=0 ; i<n ; i++){
		b[i]=0;
	}
	int r,s=0,pi,pj;
  	for (r = 0; r < (2*n-1); r++){
      	if (r<n){
	        pi=0;
	        pj=n-r-1;
	    }
	      else{
	        pi=r-(n-1);
	        pj=0;
	    }
		for (; (pi<n)&(pj<n);pi++,pj++) {
		    	if(pj>pi){
		      		b[s]+=*(a+ pi*n + pj);
		    	}
		    	if(pj == n-1){
		    		s++;
		    	}
		    		
	    }
    }
	printf(" сформований однорідний масив :\n");
	for(i=0 ; i<n-1 ; i++){
		printf("%4d",b[i]);
	}
	cout<<endl;
}
int main(){
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i,j,k,n;
	printf(" введіть розмір масиву : ");
	cin>>n;
	int a[n][n];
	input(a[0],n);
	diag(a[0],n);
	system("pause");
	return 0;
}

