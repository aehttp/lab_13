/* file name: kuts_lab_13_2 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 05.02.2022
*дата останньої зміни 05.02.2022
*Лабораторна №13
*завдання : В заданий багатовимірній матриці g19(3,6,3) знайти середнє квадратичне
значення усіх елементів. Вивести вихідну матрицю та знайдене середнє
квадратичне значення.
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
using namespace std;
void gotoxy(int x, int y){
	COORD scrn;
	HANDLE hOuput = GetStdHandle(STD_OUTPUT_HANDLE);
	scrn.X=x;
	scrn.Y=y;
	SetConsoleCursorPosition(hOuput,scrn);
}
void ser(int mas[][6][3],int n,int m, int f){
	int i,j,k ,kilk=0,sum=0;
	double s_k;
	for(i=0;i<3;i++){
		for(j=0;j<6;j++){
			for(k=0;k<3;k++){
				sum+=pow(mas[i][j][k],2);
				kilk++;
			}
		}
	}
	s_k= sqrt((double)sum/(double)kilk);
	printf("Середнє квадратичне : %f \n",s_k);
}
int main(){
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(NULL));
	int g[3][6][3];
	int i,j,k;
	printf("Сформований масив : \n");
	for(i=0;i<3;i++){
		for(j=0;j<6;j++){
			for(k=0;k<3;k++){
				g[i][j][k]= rand()%100-50;
				gotoxy(((j+k)*4+1),(((1+i)*6)-j));
				printf("%d",g[i][j][k]);
			}
			gotoxy(0,18);
			cout<<endl<<endl;
		}
	}
	ser(g,3,6,3);
	system("pause");
	return 0;
}

